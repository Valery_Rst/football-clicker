package com.example.footballclicker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView rusTeam;
    private TextView usaTeam;
    private Byte pointsOfFirstTeam = 0;
    private Byte pointsOfSecondTeam = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        rusTeam = findViewById(R.id.text_rus);
        usaTeam = findViewById(R.id.text_usa);

        ImageButton leftButton = findViewById(R.id.counter_rus);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pointsOfFirstTeam++;

                setText(rusTeam, String.valueOf(pointsOfFirstTeam));

                checkOnWinning(pointsOfFirstTeam, pointsOfSecondTeam, getString(R.string.team_RUS));
            }
        });

        ImageButton rightButton = findViewById(R.id.counter_usa);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pointsOfSecondTeam++;

                setText(usaTeam, String.valueOf(pointsOfSecondTeam));

                checkOnWinning(pointsOfSecondTeam, pointsOfFirstTeam, getString(R.string.team_USA));

            }
        });

        setText(rusTeam, String.valueOf(pointsOfFirstTeam));
        setText(usaTeam, String.valueOf(pointsOfSecondTeam));
    }

    /**
     * Метод позволяет сохранить состояние UI в объект savedInstanceState
     *
     * @param outState состояние UI
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putByte("pointsOfFirstTeam", pointsOfFirstTeam);
        outState.putByte("pointsOfSecondTeam", pointsOfSecondTeam);
    }

    /**
     * Метод используется для восстановления состояния UI
     *
     * @param savedInstanceState состояние UI
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey("pointsOfFirstTeam")) {
            pointsOfFirstTeam = savedInstanceState.getByte("pointsOfFirstTeam");
        }

        if (savedInstanceState.containsKey("pointsOfSecondTeam")) {
            pointsOfSecondTeam = savedInstanceState.getByte("pointsOfSecondTeam");
        }

        setText(rusTeam, String.valueOf(pointsOfFirstTeam));
        setText(usaTeam, String.valueOf(pointsOfSecondTeam));
    }

    public void onClickBtnReset(View view) {

        Toast.makeText(this, getString(R.string.reset), Toast.LENGTH_SHORT).show();

        pointsOfFirstTeam = 0;
        pointsOfSecondTeam = 0;

        setText(rusTeam, String.valueOf(pointsOfFirstTeam));
        setText(usaTeam, String.valueOf(pointsOfSecondTeam));
    }

    /**
     * Метод проверяет команду на предмет достаточного количества очков для выигрыша.
     *
     * В случае true - вывод всплывающего сообщения, обнуление очков
     *
     * @param winningTeamPoints количество очков команды победителей
     * @param losingTeamPoints количество очков команды проигравших
     * @param nameOfTeam название команды
     */
    private void checkOnWinning(Byte winningTeamPoints, Byte losingTeamPoints, String nameOfTeam) {

        if (winningTeamPoints == 15) {

            Toast.makeText(this,
                    nameOfTeam + getString(R.string.win_msg) + getString(R.string.spacing)
                            + winningTeamPoints + getString(R.string.separator) + losingTeamPoints
                            + getString(R.string.spacing) + getString(R.string.reset),
                    Toast.LENGTH_SHORT).show();

            pointsOfFirstTeam = 0;
            pointsOfSecondTeam = 0;

            setText(rusTeam, String.valueOf(pointsOfFirstTeam));
            setText(usaTeam, String.valueOf(pointsOfSecondTeam));
        }
    }

    /**
     * Метод предназначен для отображения счёта в TextView
     *
     * @param view элемент TextView, на котором будет отображаться счёт
     * @param pointsOfTeam количество очков команды
     */
    private void setText(TextView view, String pointsOfTeam) {
        view.setText(pointsOfTeam);
    }
}